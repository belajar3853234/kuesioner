  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url ('#')}}" class="brand-link">
      <img src="/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Kuesioner Aras</span>
    </a>

    <!-- Sidebar -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route('jamaah') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard jamaah
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('dashadmin') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard Admin
              </p>
            </a>
          <li class="nav-item">
            <a href="{{ route('data_kriteria') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Data Kriteria
              </p>
            </a>
              <li class="nav-item">
                <a href="{{ route('data_alternatif') }}" class="nav-link">
                <i class="nav-icon fas fa-chart-pie"></i>
                <p>
                    Data Alternatif
                </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('grafik_jamaah') }}" class="nav-link">
                  <i class="nav-icon fas fa-chart-pie"></i>
                  <p>
                    Grafik Kepuasan Jamaah
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('grafik_admin') }}" class="nav-link">
                <i class="nav-icon fas fa-chart-pie"></i>
                <p>
                    Grafik Kepuasan Admin
                </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('kuesioner') }}" class="nav-link"> 
                  <i class="nav-icon fas fa-chart-pie"></i>
                  <p>
                    Kuesioner
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('list_user') }}" class="nav-link"> 
                  <i class="nav-icon fas fa-chart-pie"></i>
                  <p>
                    List User
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('create_kuesioner') }}" class="nav-link"> 
                  <i class="nav-icon fas fa-chart-pie"></i>
                  <p>
                    Buat Kuesioner
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('create_kuesioner') }}" class="nav-link"> 
                  <div class="container text-center pt-4">
                    {{ auth()->user()->name }}
                    <div class="div">
                        <form action="{{ route('logout') }}" method="post">
                            @csrf
                            <input type="submit" class="btn btn-danger" value="Logout">
                        </form>
                    </div>
                </div>
                </a>
              </li>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>